package com.shack.skytrain.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.shack.skytrain.datamodel.EnumBase.*;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SyncModel {

    @JsonProperty("group")
    private GROUP group;
    @JsonProperty("action")
    private ACTION action;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("result")
    private RESULT result;
    @JsonProperty("errorCode")
    private ERRORCODE errorCode;
    @JsonProperty("payload")
    private String payload;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
