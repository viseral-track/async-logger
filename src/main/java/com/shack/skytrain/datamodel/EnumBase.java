package com.shack.skytrain.datamodel;

import lombok.AllArgsConstructor;

public class EnumBase {

    @AllArgsConstructor
    public enum ACTION {
        CREATEGAME("Create Game"),
        UPDATEGAME("Update Game"),
        CLEARGAME("Clear Game"),
        GUESSATTEMPT("Guess Attempt");

        String desc;
    }

    @AllArgsConstructor
    public enum GROUP {
        GAMEMASTER("GameMaster"),
        USER("User"),
        ADMIN("Admin");

        String desc;
    }

    @AllArgsConstructor
    public enum RESULT{
        SUCCESS("Success"),
        FAILURE("Failure");

        String desc;
    }

    @AllArgsConstructor
    public enum ERRORCODE {
        //10XXXX User input error
        //20XXXX Guess by user was wrong
        //30XXXX Dependent source is down
        SHACK_100001("Input from user was invalid"),
        SHACK_200001("User guessed wrongly"),
        SHACK_300001("Database is down");


        String errorDesc;
    }
}
