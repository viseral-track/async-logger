package com.shack.skytrain.listener;

import com.shack.skytrain.datamodel.SyncModel;
import com.shack.skytrain.jpa.model.DeltaLog;
import com.shack.skytrain.jpa.repo.DeltaRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import com.shack.skytrain.datamodel.EnumBase.*;

import java.util.concurrent.CountDownLatch;

@Component
@Slf4j
public class LogListener {

    @Autowired
    private DeltaRepo logRepo;

    private CountDownLatch latch = new CountDownLatch(1);

    @RabbitListener(queues = {"${app.rabbitmq.logQueue}"})
    public void receiveLogMessage(SyncModel message){

        Mono.just(message)
                .map(msg -> new DeltaLog(
                            msg.getGroup(),
                            msg.getAction(),
                            msg.getUserId(),
                            msg.getResult(),
                            msg.getErrorCode(),
                            null,
                            msg.getPayload())
                ).flatMap(data -> Mono.just(logRepo.save(data)))
                .doOnError(error -> {
                    log.info("Facing errors. {}", error);
                })
                .subscribe(result -> log.info("Data saved into db successfully. Data: {}", result));
    }


}
