package com.shack.skytrain;

import com.shack.skytrain.datamodel.SyncModel;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import com.shack.skytrain.datamodel.EnumBase.*;

@SpringBootApplication
public class SkytrainApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkytrainApplication.class, args);
    }

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private TopicExchange exchange;

    @Value("${rabbitmq.logRoutingKey:deltaLog}")
    private String logRoutingKey;

    @EventListener(ApplicationReadyEvent.class)
    public void sendMessage(){
        template.convertAndSend(exchange.getName(), logRoutingKey, new SyncModel(GROUP.ADMIN, ACTION.CREATEGAME, "testuser",RESULT.SUCCESS ,null, "testpayload"));
    }


}
