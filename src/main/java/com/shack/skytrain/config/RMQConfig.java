package com.shack.skytrain.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sound.midi.Receiver;

@Configuration
public class RMQConfig {

    //Default to SHACK.LOG if queue name is not provided.
    @Value("${app.rabbitmq.logQueue:SHACK.LOG}")
    private String logQueueName;

    //Default to SHACK.EXCHANGE if no exchange name is provided.
    @Value("${app.rabbitmq.logExchange:SHACK.EXCHANGE}")
    private String logExchange;

    @Value("${rabbitmq.logRoutingKey:deltaLog}")
    private String logRoutingKey;
    @Bean
    Queue logQueue(){
        return new Queue(logQueueName, true);
    }

    @Bean
    TopicExchange logExchange(){
        return new TopicExchange(logExchange);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(logRoutingKey);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory, Jackson2JsonMessageConverter converter){
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter);
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

}
