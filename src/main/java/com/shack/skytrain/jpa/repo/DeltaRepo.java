package com.shack.skytrain.jpa.repo;

import com.shack.skytrain.jpa.model.DeltaLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeltaRepo extends JpaRepository<DeltaLog, String> {
}
