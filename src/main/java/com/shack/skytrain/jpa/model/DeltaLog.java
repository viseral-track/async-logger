package com.shack.skytrain.jpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;
import com.shack.skytrain.datamodel.EnumBase.*;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tbl_delta_log")
public class DeltaLog {

    @Id
    @Column(name = "log_id")
    private String logId;

    @Column(name = "log_group")
    @Enumerated(EnumType.STRING)
    private GROUP group;

    @Column(name = "log_action")
    @Enumerated(EnumType.STRING)
    private ACTION action;

    @Column(name = "log_user_id")
    private String userId;

    @Column(name = "log_result")
    @Enumerated(EnumType.STRING)
    private RESULT result;

    @Column(name = "log_error_code")
    @Enumerated(EnumType.STRING)
    private ERRORCODE errorCode;

    @Column(name = "log_error_desc")
    private String errorDesc;

    @Column(name = "log_payload")
    private String payload;

    public DeltaLog(GROUP group, ACTION action, String userId, RESULT result, ERRORCODE errorCode, String errorDesc, String payload) {
        this.logId = UUID.randomUUID().toString();
        this.group = group;
        this.action = action;
        this.userId = userId;
        this.result = result;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.payload = payload;
    }
}
