FROM maven:3.6-jdk-12-alpine
WORKDIR /repo
ENV JARLOC="/repo/skytrain-RELEASE.jar"
COPY ./target/skytrain-RELEASE.jar  /repo
COPY ./src/main/resources/application.yml /repo
RUN ls -lrt
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar", "skytrain-RELEASE.jar"]
#EXPOSE 8080
#CMD ["java -jar deploy.jar"]